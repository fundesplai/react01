import React, { useState } from "react"; // hook
import './estilos.css';
import { Button } from 'reactstrap';




const Botoncillo = () => {
    const [contador, setContador] = useState(1);
    const clicar = () => setContador(contador + 1);
    return (
        <Button onClick={clicar} color="warning">Valor {contador}</Button>
    );
}


const Bola = () => {

    const [contador, setContador] = useState(10);

    const clicar = () => setContador(contador + 1);

    return (
        <div className="bola" onClick={clicar} >{contador}</div>
    );
}


const Cabecera = (props) => (
    <div className="cabecera">
        {props.titulo}---{props.num}
    </div>
);

class CabeceraClass extends React.Component {
    render() {
        return (
            <div className="cabecera">
                {this.props.titulo}
            </div>
        )
    }
}

const Pie = ({ titulo }) => (
    <div className="pie">
        <p>{titulo}</p>
    </div>
);

const Contenido = (props) => (
    <div className="contenido">
        <h1>hola</h1>
        {props.children}
        <h3>adios</h3>
    </div>
);



const Bandera = (props) => {

    const colorIcono = (props.alerta) ? "red" : "green";

    return (
        <i style={{color: colorIcono, fontSize: "30px"}} class="fa fa-flag" aria-hidden="true"></i>
    )
};


const Banyera = (props) => {

  

    return (
        <i style={{color: props.colorIcono, fontSize: "30px"}} className="fa fa-bath" aria-hidden="true"></i>
    )
};

export { Cabecera, Pie, Contenido, CabeceraClass, Bola, Botoncillo, Banyera, Bandera };
