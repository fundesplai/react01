import React from "react";

import 'bootstrap/dist/css/bootstrap.min.css';

import { Cabecera, Pie, Contenido, Bola, Botoncillo, Banyera, Bandera } from './Componentes';


export default () => {
  return (
    <div>
      <Cabecera titulo="Mi tercera aplicación REACT JS" num="22" />
      <Contenido>
        <Botoncillo />
        
        <Bola />
        <table className="table">
        <tr>
          <td>Hola</td>
          <td><Bandera alerta /></td>
          <td><Banyera colorIcono="red" /></td>
        </tr>
        
        <tr>
          <td>Hola</td>
          <td><Bandera /></td>
          <td><Banyera colorIcono="green" /></td>
        </tr>
        </table>
        <img src="http://placekitten.com/200/200" />
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In ipsa ut placeat iusto laudantium est id temporibus officiis obcaecati excepturi cupiditate repellat maxime voluptates, expedita, quo quidem. Error, sequi tenetur?</p>
      </Contenido>
      <Pie titulo="Footer" />
    </div>
  );
};
